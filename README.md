Atos OPTICA Code Exercise 1
A REST java application using springboot gradle and swagger.
============================================================

https://gitlab.com/wilkinson.philip/atosopticatest

Requires java 8 installed. port 8080 unblocked, and a browser.
gradle is provided by the project.

Build with gradle, e.g.
C:\Data\dev\projects\atosopticatest>gradlew clean build

Run unit tests in gradle, e.g.
C:\Data\dev\projects\atosopticatest>gradlew clean test

Run integration test manually from IDE.
see CustomerControllerIntegrationTest

To run with gradle, e.g.
C:\Data\dev\projects\atosopticatest>gradlew bootRun

The 3 required REST operation endpoints are..  
POST - http://localhost:8080/customer/add - adds a customer  
GET - http://localhost:8080/customer/list - lists all customers  
DELETE - http://localhost:8080/customer/remove/{id} - removes a single customer  

A read of a single customer was not required, so not provided.  

See swagger ui to run from browser..
http://localhost:8080/swagger-ui.html#/





