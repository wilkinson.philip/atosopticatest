package atosoptictest.model;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    @Test
    public void getId() {
        Customer test1 = new Customer("a", "b");
        Assert.assertNotNull(test1.getId());
        Customer test2 = new Customer("a", "b");
        Assert.assertNotNull(test2.getId());
        Assert.assertNotEquals(test1.getId(), test2.getId());
    }
}