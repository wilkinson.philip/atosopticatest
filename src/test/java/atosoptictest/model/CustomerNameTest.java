package atosoptictest.model;

import org.junit.Assert;
import org.junit.Test;

public class CustomerNameTest {

    @Test(expected = AssertionError.class)
    public void allNull() {
        new CustomerName(null, null);
    }

    @Test(expected = AssertionError.class)
    public void allTroSmall() {
        new CustomerName("", "");
    }

    @Test(expected = AssertionError.class)
    public void firstNameIsNull() {
        new CustomerName(null, "b");
    }

    @Test(expected = AssertionError.class)
    public void firstNameToSmall() {
        new CustomerName("", "b");
    }

    @Test(expected = AssertionError.class)
    public void firstNameToBig() {
        new CustomerName("123456789012345678901", "b");
    }

    @Test(expected = AssertionError.class)
    public void secondNameIsNull() {
        new CustomerName("a", null);
    }

    @Test(expected = AssertionError.class)
    public void secondNameToSmall() {
        new CustomerName("a", "");
    }

    @Test(expected = AssertionError.class)
    public void secondNameToBig() {
        new CustomerName("a", "123456789012345678901");
    }

    @Test
    public void getName() {

        System.out.println("Running getName unit test");

        CustomerName test = new CustomerName("a", "b");
        Assert.assertEquals("a", test.getFirstname());
        Assert.assertEquals("b", test.getSurname());
    }
}