package atosoptictest;

import atosoptictest.model.Customer;
import atosoptictest.model.CustomerName;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

/**
 * Integration test to test the controller. -- RUN MANUALLY
 */
@Ignore // Ignore integration test - run manually
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CustomerControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void testAll() throws Exception {

        System.out.println("Running controller integration test");

        final String json1 = objectMapper.writeValueAsString(new CustomerName("1stName", "2ndName"));
        final String json2 = objectMapper.writeValueAsString(new CustomerName("test1stName", "test2ndName"));

        // Add 1stName, 2ndName
        mvc.perform(
                post("/customer/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json1)

        ).andExpect(status().isCreated());

        // List customers, check "1stName, 2ndName" in list
        final MvcResult result = mvc.perform(
                get("/customer/list")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstname", is("1stName")))
                .andExpect(jsonPath("$[0].surname", is("2ndName")))
        .andReturn();

        // Check unmarshall
        final Customer[] customerResults = objectMapper.readValue(result.getResponse().getContentAsString(), Customer[].class);

        Assert.assertNotNull(customerResults);
        Assert.assertEquals(1, customerResults.length);
        Assert.assertEquals("1stName", customerResults[0].getFirstname());
        Assert.assertEquals("2ndName", customerResults[0].getSurname());
        Assert.assertNotNull(customerResults[0].getId());

        // Add second customer. test1stName, test2ndName
        mvc.perform(
                post("/customer/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json2)

        ).andExpect(status().isCreated());

        // Check both customers now in list
        mvc.perform(
                get("/customer/list")
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].firstname", is("1stName")))
                .andExpect(jsonPath("$[0].surname", is("2ndName")))
                .andExpect(jsonPath("$[1].firstname", is("test1stName")))
                .andExpect(jsonPath("$[1].surname", is("test2ndName")));

        // remove the 1st customer "1stName, 2ndName"
        mvc.perform(
                delete("/customer/remove/" + customerResults[0].getId())
        )
                .andExpect(status().isOk());

        // list again to check that the 1st customer has been removed and only the second customer remains.
        mvc.perform(
                get("/customer/list")
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstname", is("test1stName")))
                .andExpect(jsonPath("$[0].surname", is("test2ndName")));
    }
}