package atosoptictest;

import atosoptictest.model.Customer;
import atosoptictest.model.CustomerName;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test to test the controller.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CustomerService service;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void add() throws Exception {
        CustomerName customer = new CustomerName("a", "b");
        final String json = objectMapper.writeValueAsString(customer);
        BDDMockito.given(service.addCustomer("a", "b")).willReturn(new Customer("a", "b"));
        mvc.perform(
                post("/customer/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)

        ).andExpect(status().isCreated());
    }

    @Test
    public void list() throws Exception {
        Customer customer = new Customer("a", "b");
        Collection<Customer> customers = Arrays.asList(customer);
        BDDMockito.given(service.listCustomers()).willReturn(customers);

        mvc.perform(
                get("/customer/list")
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstname", is("a")))
                .andExpect(jsonPath("$[0].surname", is("b")));
    }

    @Test
    public void remove() throws Exception {
        BDDMockito.given(service.removeCustomer("1")).willReturn(true);
        mvc.perform(
                delete("/customer/remove/1")
        )
        .andExpect(status().isOk());
    }

    @Test
    public void removeFail() throws Exception {
        BDDMockito.given(service.removeCustomer("1")).willReturn(false);
        mvc.perform(
                delete("/customer/remove/1")
        )
                .andExpect(status().isNotFound());
    }

}