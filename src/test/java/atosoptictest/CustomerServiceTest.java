package atosoptictest;

import atosoptictest.model.Customer;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import static org.junit.Assert.*;

public class CustomerServiceTest {

    @Test
    public void addCustomer() {
        CustomerService test = new CustomerService();
        test.addCustomer("a", "b");
        Collection<Customer> customers = test.listCustomers();
        Assert.assertEquals(1, customers.size());
        Customer customer = customers.iterator().next();
        Assert.assertEquals("a", customer.getFirstname());
        Assert.assertEquals("b", customer.getSurname());
    }

    @Test
    public void getCustomer() {
        CustomerService test = new CustomerService();
        test.addCustomer("a", "b");
        Collection<Customer> customers = test.listCustomers();
        Assert.assertEquals(1, customers.size());
        Customer customer = customers.iterator().next();
        Optional<Customer> gotCustomer = test.getCustomer(customer.getId());
        Assert.assertTrue(gotCustomer.isPresent());
        Assert.assertEquals(customer, gotCustomer.get());

        Optional<Customer> notGotCustomer = test.getCustomer("123");
        Assert.assertFalse(notGotCustomer.isPresent());
    }

    @Test
    public void removeCustomer() {
        CustomerService test = new CustomerService();
        test.addCustomer("a", "b");
        Collection<Customer> customers1 = test.listCustomers();
        Assert.assertEquals(1, customers1.size());
        Customer customer1 = customers1.iterator().next();

        test.removeCustomer(customer1.getId());

        Collection<Customer> customers2 = test.listCustomers();
        Assert.assertEquals(0, customers2.size());
    }

    @Test
    public void listCustomers() {
        CustomerService test = new CustomerService();
        test.addCustomer("a", "b");
        test.addCustomer("c", "d");
        test.addCustomer("e", "f");

        Collection<Customer> customers = test.listCustomers();
        Assert.assertEquals(3, customers.size());

        Iterator<Customer> iter = customers.iterator();
        Customer customer1 = iter.next();
        Customer customer2 = iter.next();
        Customer customer3 = iter.next();

        Assert.assertEquals("a", customer1.getFirstname());
        Assert.assertEquals("b", customer1.getSurname());
        Assert.assertEquals("c", customer2.getFirstname());
        Assert.assertEquals("d", customer2.getSurname());
        Assert.assertEquals("e", customer3.getFirstname());
        Assert.assertEquals("f", customer3.getSurname());
    }
}