package atosoptictest;

import atosoptictest.model.Customer;
import atosoptictest.model.CustomerName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("customer")
@Api(description="The Customer REST API Controller")
public class CustomerController {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException
    {
        static final long serialVersionUID = 1L;
        ResourceNotFoundException(String msg) {super(msg);}
    }

    private final static Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerService customerService;

    @RequestMapping(
            value = "add",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            method = {RequestMethod.POST})
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @ApiOperation("Adds a new customer given a firstname and surname, returns customer with id")
    public Customer add(@RequestBody final CustomerName customer) {
        LOGGER.debug("create customer called");
        final Customer newCustomer = customerService.addCustomer(customer.getFirstname(), customer.getSurname());
        return newCustomer;
    }

    @RequestMapping(
            value = "list",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation("Lists all the customers")
    public Collection<Customer> list() {
        LOGGER.debug("list customers called");
        return customerService.listCustomers();
    }

    @RequestMapping(
            value = "remove/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            method = {RequestMethod.DELETE})
    @ApiOperation("removes the customer identified by the id path parameter")
    public void remove(@PathVariable("id") final String id) {
        LOGGER.debug("remove customer called");
        if (false == customerService.removeCustomer(id)) {
            throw new ResourceNotFoundException("Could not remove customer - id = " + id + " not found");
        }
    }

    // No get required.
}
