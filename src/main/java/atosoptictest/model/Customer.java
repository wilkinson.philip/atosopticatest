package atosoptictest.model;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class Customer extends CustomerName {

    @NotNull
    private final String id;

    public Customer(final String firstname, final String surname) {
        super(firstname, surname);
        this.id = UUID.randomUUID().toString().replaceAll("-","");
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Customer) {
            return super.equals(obj) && this.getId().equals(((Customer)obj).getId());
        }
        return false;
    }
}
