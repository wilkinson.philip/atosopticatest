package atosoptictest.model;

import org.springframework.util.StringUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CustomerName {

    @NotBlank
    @Size(min = 1, max = 20)
    private final String firstname;

    @NotBlank
    @Size(min = 1, max = 20)
    private final String surname;

    /**
     * constructs a CustomerName object with firstname and surname
     * @param firstname the first name
     * @param surname the surname
     */
    public CustomerName(final String firstname, final String surname) {
        assert !StringUtils.isEmpty(firstname) : "firstname must not be null or empty";
        assert firstname.length() <= 20 : "firstname must be less than or equal to 20 characters";

        assert !StringUtils.isEmpty(surname) : "surname must not be null or empty";
        assert surname.length() <= 20 : "surname must be less than or equal to 20 characters";

        this.firstname = firstname;
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getSurname() {
        return surname;
    }
}
