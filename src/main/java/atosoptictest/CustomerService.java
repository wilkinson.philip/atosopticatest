package atosoptictest;

import atosoptictest.model.Customer;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CustomerService {

    /** the cache of Customers - not persisted */
    private final Map<String, Customer> cache = new LinkedHashMap<>(); // retain order added.

    /**
     * add a customer to the cache.
     * @param firstname the firstname
     * @param surname the surname
     * @return the new Customer object (with id) added to cache.
     */
    public Customer addCustomer(final String firstname, final String surname) {
        final Customer newCustomer = new Customer(firstname, surname);
        cache.put(newCustomer.getId(), newCustomer);
        return newCustomer;
    }

    /**
     * gets a single customer given the id.
     * @param id the id.
     * @return optionally - the customer if found.
     */
    public Optional<Customer> getCustomer(final String id) {
        return Optional.ofNullable(cache.get(id));
    }

    /**
     * removes a single customer from the cache.
     * @param id the id.
     * @return true if this operation found and removed the customer identified by <code>id</code>
     */
    public boolean removeCustomer(final String id) {
        boolean ret = false;
        if (cache.containsKey(id)) {
            cache.remove(id);
            ret = true;
        }
        return ret;
    }

    /**
     * lists all the customers in the cache in the order they were added.
     * @return all the customers.
     */
    public Collection<Customer> listCustomers() {
        return cache.values();
    }
}
